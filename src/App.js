import React, { Component } from 'react'
import Products from './components/Products'
import NavBar from './components/NavBar'
import Error from './components/Error';
import PreLoader from './components/PreLoader';
import Footer from './components/Footer';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        }

        this.state = {
            product: [],
            status: this.API_STATES.LOADING
        };

        this.URL = 'https://fakestoreapi.com/products';
    }

    fetchData(url){
        fetch(url)
            .then(resp => resp.json())
            .then((data) => {
                this.setState({
                    product: data,
                    status: this.API_STATES.LOADED
                });
            })
            .catch((err) => {
                this.setState({
                    status : this.API_STATES.ERROR
                })
            });
    }

    componentDidMount() {
        this.fetchData(this.URL)
    }

    render() {
        return (
            <div className='App'>
                <NavBar />
                {
                    this.state.status === this.API_STATES.ERROR?
                        <Error /> : <>
                            {this.state.status ===  this.API_STATES.LOADING ? <PreLoader /> :
                                <Products products={this.state.product} />}
                        </>
                }
                <Footer />
            </div>
        )
    }
}
