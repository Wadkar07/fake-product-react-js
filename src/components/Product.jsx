import React, { Component } from 'react'
import './Product.css'

export default class Product extends Component {

    render() {
        let product = this.props.product;
        return (
            <div className="products-card">
                <img src={product.image} alt="" className="product-image" />
                <div className="category">🏷️ {product.category}</div>
                <h4 className="product-name">
                    {product.title}
                </h4>
                <p className="price">💲 {product.price}</p>
                <p className="rating">Rating : {product.rating.rate} ⭐ ({product.rating.count})</p>
            </div>
        )
    }
}