import React, { Component } from 'react'
import './Footer.css'

export class Footer extends Component {
  render() {
    return (
      <div className='footer'>
        All rights are reserved
      </div>
    )
  }
}

export default Footer
