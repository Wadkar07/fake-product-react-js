import React, { Component } from 'react'
import Product from './Product'
import './Products.css'

export class Products extends Component {
  render() {
    return(
      <div className="products">
        {
          this.props.products.map((product,index) => {
            return (
                <Product product={product} key={index}/>
            )
          })
      }
      </div>
    )
  }
}

export default Products
