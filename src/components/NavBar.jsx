import React, { Component } from 'react'
import './NavBar.css'
import logo from '../images/logo.png'

export class NavBar extends Component {
  render() {
    return (
      <div className='navbar'>
        <img src={logo} alt="" className="logo-image" />
      </div>
    )
  }
}

export default NavBar
