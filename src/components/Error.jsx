import React, { Component } from 'react'
import './Error.css'

export class Error extends Component {
  render() {
    return (
      <div className='error'>
        <h1>We are facing some Issue</h1>
      </div>
    )
  }
}

export default Error
